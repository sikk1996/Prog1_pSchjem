package Oblig1.Modul2.Fjern_tekst_fra_filen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class RemoveStrings {

	public static void main(String[] args) { //"ost" tekst.txt
		if(args.length>2 || args.length ==0) {
			System.out.println("Usage: java RemoveStrings stringToBeRemoved sourceFile");
			System.exit(1);
		}

		try {
			//System.out.println(createCleanString(args));
			args[1]="src\\"+args[1];
			printToFile(args[1],createCleanString(args));
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Usage: java RemoveStrings stringToBeRemoved sourceFile");
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.out.println("Source file "+args[1]+" not exist");
			System.exit(2);
		}

	}
	private static String createCleanString(String[] args) throws FileNotFoundException {
		String buffer= "";
		System.out.println("Reading and removing string from file");
		Scanner filAvleser = new Scanner(new File(args[1]));
		while(filAvleser.hasNextLine()){
			buffer += filAvleser.nextLine() + "\n";
		}
		buffer = buffer.replaceAll(args[0],"");
		System.out.println("Done!");
		return buffer;
	}
	private static void printToFile(String fileName,String text) throws FileNotFoundException,ArrayIndexOutOfBoundsException {
		File file = new File(fileName);
		PrintWriter writer;
		System.out.println("Writing back to file");
		System.out.println(text);
		writer  = new PrintWriter(file);
		writer.print(text);
		writer.close();



		System.out.println("Done!");
	}
}
