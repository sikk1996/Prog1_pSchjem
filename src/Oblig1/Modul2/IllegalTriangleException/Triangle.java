package Oblig1.Modul2.IllegalTriangleException;

import java.rmi.server.ExportException;

public class Triangle extends GeometricObject {
	private double side1;
	private double side2;
	private double side3;

	public Triangle() throws IllegalTriangleException{
		this(1,1,1);
	}
	public Triangle(double side) throws IllegalTriangleException{
		this(side,side,side);
	}
	public Triangle(double side1, double side2, double side3) throws IllegalTriangleException{
		super();
		validateTriangle(side1,side2,side3);
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = side3;
	}
	public Triangle(double side1, double side2, double side3, String color, boolean fill) throws IllegalTriangleException{
		this(side1,side2,side3);
		super.setColor(color);
		super.setFilled(fill);
		validateTriangle(side1,side2,side3);
	}

	public double getArea() {
		double s = (side1+side2+side3)/2.0;
		return Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
	}
	public double getPerimeter() {
		return side1+side2+side3;
	}

	private void validateTriangle(double side1,double side2,double side3)throws IllegalTriangleException{
		if(side1<=0 || side2<=0 || side3<=0) {                                                          //En trekant er gyldig når alle sider har positivt lengde.
			throw new IllegalTriangleException("En av sidene er 0 eller negativ",side1,side2,side3);
		} else if (!(side1 < side2+side3 && side2 < side1+side3 && side3 < side2+side1)) {              //og enhver side er mindre enn summen av to gjenstående sider
			throw new IllegalTriangleException("En av sidene er for lange",side1,side2,side3);
		}
	}
	@Override
	public String toString() {
		return "\nArea: "+getArea()+"\nPerimeter: "+getPerimeter()+"\n\nSides: "+side1+", "+side2+", "+side3+"\n"+super.toString();
	}

}
