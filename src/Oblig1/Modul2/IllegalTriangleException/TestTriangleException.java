package Oblig1.Modul2.IllegalTriangleException;

public class TestTriangleException {
	public static void main(String[] args) {
		Triangle correctTriangle = createTriangle(1.5,2,3);
		Triangle invalidTriangle = createTriangle(-2,3,4);
		Triangle invalidTriangle2 = createTriangle(10,1,1);


	}
	public static Triangle createTriangle(double side1,double side2,double side3) {
		Triangle triangle;
		try {
			System.out.printf("\nCreating triangle with sides: %.2f %.2f %.2f\n",side1,side2,side3);
			triangle = new Triangle(side1,side2,side3);
			System.out.printf("Perimeter: %.2f\nArea: %.2f\n",triangle.getPerimeter(),triangle.getArea());
			return triangle;
		} catch(IllegalTriangleException e){
			System.out.println(e.getMessage());
			System.out.println("Side 1: "+e.getSide1());
			System.out.println("Side 2: "+e.getSide2());
			System.out.println("Side 3: "+e.getSide3());
			return null;
		}

	}
}
