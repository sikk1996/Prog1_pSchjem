package Oblig1.Modul2.ArrayIndexOutOfBounds;

import java.util.Scanner;

public class TestArrayBounds {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] mittArray = new int[100];
		int indeks;

		for (int i =0;i<mittArray.length;i++) {
			mittArray[i] = (int)(Math.random()*10000);
		}

		System.out.println("Skriv inn indeks: ");
		indeks = input.nextInt();
		try {
			System.out.println(mittArray[indeks]);
		} catch(Exception ArrayIndexOutOfBoundsException) {
			System.out.println("Out of Bounds");
		}


	}
}
