package Oblig1.Modul2.Les_resultater;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadScores {
	public static void main(String[] args) {
		File file;
		Scanner input = new Scanner(System.in);
		double sum=0;
		String fileName;
		int numbersOfNumbers=0;

		//Be brukeren om å oppgi filnavnet for filen med resultater.
		System.out.println("Oppgi filnavnet for fil: ");
		fileName = "src/"+input.nextLine();

		//Åpne filen og lese innholdet i den ved hjelp av Scanner, du skal bruke try-with-resources.
		file = new File(fileName);

		try (Scanner filAvleser = new Scanner(file)) {
			//Scanner filAvleser = new Scanner(file);

			while (filAvleser.hasNext()){
				sum += Double.parseDouble(filAvleser.next());   //Summere alle resultater.
				numbersOfNumbers++;
			}

			//Skrive ut summen og gjennomsnittet til skjerm.
			System.out.println("Antall tall i fil: "+numbersOfNumbers);
			System.out.println("Sum av alle tall: "+sum);
			System.out.println("Gjenomsnitt av tallene: "+sum/numbersOfNumbers);

		} catch (FileNotFoundException e){
			System.out.println("Filen ble ikke funnet, error: "+e.getMessage());
		}

	}
}
