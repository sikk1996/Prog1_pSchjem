package Oblig1.Modul1.ArrayList_Operasjoner;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayListOperations {
	public static void main(String[] args) {
		//Create array and list
		int[] integerArray = {1,2,3,4,5,6,7,8,9,10};
		ArrayList<Integer> integerArrayList = new ArrayList<>(0);

		//Convert Array to ArrayList
		for (int i=0;i<integerArray.length;i++) {
			integerArrayList.add(integerArray[i]);
		}

		//Print list, add 11 and remove 3, print again
		System.out.println(integerArrayList.toString());
		integerArrayList.add(11);
		integerArrayList.remove(integerArrayList.indexOf(3));
		System.out.println(integerArrayList.toString());

		//Does list contain x?
		System.out.println("Does list contain 11? "+(integerArrayList.contains(11)?"yes":"no"));
		System.out.println("Does list contain  3? "+(integerArrayList.contains(3)?"yes":"no"));

		//Shuffle-time
		integerArrayList = shuffleArrayList(integerArrayList);
		System.out.println("Shuffled: "+integerArrayList.toString());

		//Sort
		Collections.sort(integerArrayList,Collections.reverseOrder());
		System.out.println("Sorted: "+integerArrayList.toString());


	}
	public static ArrayList<Integer> shuffleArrayList(ArrayList<Integer> oldList) {
		ArrayList<Integer> tempList = new ArrayList<>(0);
		while(oldList.size()>0) {
			int pick = (int)(Math.random()*oldList.size());
			tempList.add(oldList.get(pick));
			oldList.remove(pick);
		} return tempList;
	}
}
