package Oblig1.Modul1.TriangleKlassen;

public class Triangle extends GeometricObject{
	private double side1;
	private double side2;
	private double side3;

	public Triangle(){
		super();
		side1 = 1;
		side2 = 1;
		side3 = 1;
	}
	public Triangle(double side){
		super();
		side1 = side;
		side2 = side;
		side3 = side;
	}
	public Triangle(double side1, double side2, double side3){
		super();
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = side3;
	}
	public Triangle(double side1, double side2, double side3, String color, boolean fill){
		super(color,fill);
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = side3;
	}

	public double getArea() {
		double s = (side1+side2+side3)/2.0;
		return Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
	}
	public double getPerimeter() {
		return side1+side2+side3;
	}
	@Override
	public String toString() {
		return "\nArea: "+getArea()+"\nPerimeter: "+getPerimeter()+"\n\nSides: "+side1+", "+side2+", "+side3+"\n"+super.toString();
	}


}
