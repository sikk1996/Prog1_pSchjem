package Oblig1.Modul1.EqualsInTriangle;

public class Triangle {
	private int sides;
	public Triangle(int sides) {
		this.sides = sides;
	}

	public int getSides() {
		return sides;
	}

	public void setSides(int sides) {
		this.sides = sides;
	}

	public boolean equals(Triangle other){
		if (this.sides == other.getSides()){
			return true;
		}
			return false;
	}
}
