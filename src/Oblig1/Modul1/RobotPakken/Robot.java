package Oblig1.Modul1.RobotPakken;

public class Robot {
	private String name;
	private int distanceWalked;
	private char currentHeading;
	private int xCoord=0;
	private int yCoord=0;

	private static final char North ='N';
	private static final char West ='W';
	private static final char East ='E';
	private static final char South ='S';

	public Robot(String name,char currentHeading) {
		this.name = name;
		this.currentHeading = currentHeading;
	}
	public Robot(String name) {
		this.name = name;
		currentHeading = North;
	}

	public String getName() {
		return name;
	}

	public int getDistanceWalked() {
		return distanceWalked;
	}

	public char getCurrentHeading() {
		return currentHeading;
	}

	public int getxCoord() {
		return xCoord;
	}

	public int getyCoord() {
		return yCoord;
	}

	public void Walk() {
		distanceWalked+=1;
		switch(currentHeading) {
			case 'N':
				yCoord+=1;
				break;
			case 'W':
				xCoord-=1;
				break;
			case 'S':
				yCoord-=1;
				break;
			case 'E':
				xCoord+=1;
				break;
		}
	}
	public void TurnLeft() {
		switch(currentHeading) {
			case 'N':
				currentHeading=West;
				break;
			case 'W':
				currentHeading =South;
				break;
			case 'S':
				currentHeading=East;
				break;
			case 'E':
				currentHeading=North;
				break;
		}
	}
	public void TurnRight() {
		switch(currentHeading) {
			case 'N':
				currentHeading=East;
				break;
			case 'W':
				currentHeading =North;
				break;
			case 'S':
				currentHeading=West;
				break;
			case 'E':
				currentHeading=South;
				break;
		}
	}

}
