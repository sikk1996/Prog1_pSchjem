package Oblig1.Modul1.RobotPakken;

public class testRobot {
	public static void main(String[] args) {
		Robot Securibot = new Robot("ROBOCOP",'N');
		Securibot.Walk();
		Securibot.TurnRight();

		Securibot.Walk();
		Securibot.TurnLeft();

		Securibot.Walk();
		Securibot.Walk();
		Securibot.TurnRight();

		Securibot.Walk();
		Securibot.Walk();
		Securibot.TurnRight();

		Securibot.Walk();
		Securibot.Walk();
		Securibot.Walk();
		Securibot.TurnRight();

		Securibot.Walk();
		System.out.println("I AM "+Securibot.getName()+"\nDistance walked: "+Securibot.getDistanceWalked()+". Facing: "+Securibot.getCurrentHeading()+"\nCoordinates: "+Securibot.getxCoord()+":"+Securibot.getyCoord());
	}
}


