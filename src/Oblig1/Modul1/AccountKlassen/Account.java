package Oblig1.Modul1.AccountKlassen;

import java.util.ArrayList;
import java.util.Date;

public class Account {
	public static final char withdrawal = 'w';
	public static final char deposit = 'd';

	private static double  annualInterestRate;
	private Date dateCreated;
	private double balance;
	private String name;
	private int id;
	private ArrayList<Transaction> transactions = new ArrayList<Transaction>(0);
	private static int newID;

	public Account() {
		this("Intet navn",newID,2000);
	}
	public Account(String name, int id, double balance) {
		newID++;
		this.name=name;
		this.id=id;
		this.balance=balance;
		dateCreated = new Date();
	}

	public ArrayList<Transaction> getTransaction() {
		return transactions;
	}

	public double getAnnualInterestRate() {
		return annualInterestRate;
	}
	public double geMonthlyInterestRate() {
		return annualInterestRate/12;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public double getBalance() {
		return balance;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public void setAnnualInterestRate(double annualInterestRate) {
		Account.annualInterestRate = annualInterestRate;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void withdraw(double amount, String name) {

		if (balance>amount) {
			balance -= amount;
		}

		transactions.add(new Transaction(withdrawal,amount,balance,name));
	}
	public void deposit(double amount, String name) {

		if(amount>0) {
			balance += amount;
		}

		transactions.add(new Transaction(deposit,amount,balance,name));
	}


}
