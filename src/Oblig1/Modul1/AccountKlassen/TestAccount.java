package Oblig1.Modul1.AccountKlassen;

import java.util.ArrayList;

public class TestAccount {
	public static void main(String[] args) {
		Account peter = new Account("Peter",1,2000);
		peter.withdraw(200,"Bolle i ovnen");
		peter.deposit(400,"Lønn");
		peter.withdraw(20,"parkering");
		peter.withdraw(1000,"Joikeboller på statoil");
		peter.deposit(40,"penger fra farfar");
		peter.deposit(1000,"skattepenger");

		printTransactions(peter);

	}
	public static void printTransactions(Account account) {
		ArrayList<Transaction> transactions= account.getTransaction();

		System.out.println("Name: "+account.getName());
		System.out.println("Annual interest rate: "+account.getAnnualInterestRate());
		System.out.println("Balance: "+account.getBalance());
		System.out.println("Date                           Type   Amount    Balance   Description");
		for (int i=0;i<transactions.size();i++) {
			System.out.println(transactions.get(i).toString());
		}
	}
}
