package Oblig1.Modul1.UniversitetDB;

public class testPersons {
	public static void main(String[] args) {
		Student psc007 = new Student("peter", "schjem", "Reinslettveien 27A 8009 Bodø", "90889646", "peter@schjem.net", 1, Student.compEngineer);
		Faculty helgeFredriksen = new Faculty("Helge", "Fredriksen", "Sjøgata 1 8009 bodø", "1881", "helgefredriksen@uit.no", 1000000, Faculty.Professor, "Oppi skogen");
		Staff joakimBroks = new Staff("Joakim", "Broks", "løpsmarka 1 bodø", "+69 6969 600", "jokke@broks.xx", 400000, "Vekter");
		System.out.println(psc007.toString());
		System.out.println(helgeFredriksen.toString());
		System.out.println(joakimBroks.toString());

		joakimBroks.setSalary(450000);
		System.out.println("\n" + joakimBroks.getSalary());
		psc007.setFirstname("Peter");
		psc007.setLastname("Schjem");
		psc007.setAdress("Bruveien 1, 8009 Bodø");
		System.out.println(psc007.toString());
	}
}