package Oblig1.Modul1.UniversitetDB;

public class Staff extends Employee {
	private String title;

	/*public Staff() {
		super();
		title = "Ukjent";
	}*/
	public Staff(String firstname,String lastname,String adress,String phoneNumber,String email, double salary, String title) {
		super(firstname, lastname, adress, phoneNumber, email, salary);
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "\nStaff:"+"\nStilling: "+title+super.toString();
	}
}
