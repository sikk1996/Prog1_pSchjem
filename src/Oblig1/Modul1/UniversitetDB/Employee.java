package Oblig1.Modul1.UniversitetDB;

import java.util.GregorianCalendar;

public class Employee extends Person {

	private java.util.GregorianCalendar dateCreated;
	private double salary;

	/*public Employee(){
		super();
		dateCreated = new java.util.GregorianCalendar();
		this.salary = 0;
	}*/
	public Employee(String firstname,String lastname,String adress,String phoneNumber,String email, double salary) {
		super(firstname, lastname, adress, phoneNumber, email);
		dateCreated = new java.util.GregorianCalendar();
		this.salary = salary;
	}

	public double getSalary() {
		return salary;
	}

	public GregorianCalendar getDateCreated() {
		return dateCreated;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "\n"+"Årslønn: "+salary+"\n"+super.toString();
	}
}
