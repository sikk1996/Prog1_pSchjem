package Oblig1.Modul1.UniversitetDB;

public class Student extends Person {
	private int year;
	private int course;

	public static final int preEngineer = 0;
	public static final int compEngineer = 1;
	public static final int nurse = 2;
	public static final int police = 3;


	/*public Student(){
		super();
		this.year = 0;
		this.course = 999;
	}*/

	/*public Student(String firstname,String lastname,String adress,String phoneNumber,String email) {
		super(firstname, lastname, adress, phoneNumber, email);
		this.year = 0;
		this.course = 999;
	}*/

	public Student(String firstname,String lastname,String adress,String phoneNumber,String email,int year, int course) {
		super(firstname, lastname, adress, phoneNumber, email);
		this.year = year;
		this.course = course;
	}

	public int getYear() {
		return year;
	}

	public int getCourse() {
		return course;
	}

	private String getCourseAsString(int course){
		switch (course) {
			case 0:
				return "Forkurs ingeniør";
			case 1:
				return "Ingeniør, datateknikk";
			case 2:
				return "Sykepleier";
			case 3:
				return "Politi";
			default:
				return "Ukjent studium";
		}
	}

	@Override
	public String toString() {
		return "\nStudent:\n"+"År: "+year+"\nStudieretning: "+getCourseAsString(course)+"\n"+super.toString();
	}
}
