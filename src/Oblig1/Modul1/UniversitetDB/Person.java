package Oblig1.Modul1.UniversitetDB;

public class Person {

	private String firstname;
	private String lastname;
	private String adress;
	private String phoneNumber;
	private String email;

	/*public Person() {
		this("---","---","---","---","---");
	}*/
	public Person(String firstName,String lastname,String adress,String phoneNumber,String email) {
		this.firstname = firstName;
		this.lastname = lastname;
		this.adress = adress;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}

	public String getAdress() {
		return adress;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Navn: "+firstname+" "+lastname+"\nAdresse: "+adress+"\nTelefon nummer: "+phoneNumber+"\nEmail: "+email;
	}

}
