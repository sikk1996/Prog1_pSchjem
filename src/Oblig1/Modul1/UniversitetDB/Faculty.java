package Oblig1.Modul1.UniversitetDB;

public class Faculty extends Employee {
	private String office;
	public static final int StudAss = 1;
	public static final int Lektor = 2;
	public static final int Professor = 3;
	public static final int Forsker = 4;
	private int rank;

	/*public Faculty() {
		super();
		this.office = "ukjent";
		this.rank = 0;
	}*/
	public Faculty(String firstname,String lastname,String adress,String phoneNumber,String email, double salary,int rank, String office) {
		super(firstname, lastname, adress, phoneNumber, email, salary);
		this.rank = rank;
		this.office = office;
	}
	private String getRankAsString(int rank) {
		switch (rank) {
			case 1:
				return "Student Assistent";
			case 2:
				return "Lektor";
			case 3:
				return "Professor";
			case 4:
				return "Forsker";
			default:
				return "Ukjent";
		}
	}

	@Override
	public String toString() {
		return "\nFaculty:\n"+"Kontor: "+office+"\nRang: "+getRankAsString(rank)+super.toString();
	}
}
