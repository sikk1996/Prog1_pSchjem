package Oblig4.ReversereHeltall;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class ReverseInt extends Application{
	private StringBuilder sb = new StringBuilder();

	public void start(Stage primaryStage) throws Exception {

		VBox vbox = new VBox(25);
		vbox.setAlignment(Pos.CENTER);

		TextField input = new TextField();
		Button btnReverse = new Button("Reverser");
		TextField output = new TextField();
		output.setEditable(false);
		input.setText("Skriv integer her");

		btnReverse.setOnAction(event ->{
			output.setText(" ");
			output.setText(""+reverseInteger((Integer.parseInt(input.getText()))));
		});
		Pane pane = new FlowPane();
		pane.getChildren().addAll(input,btnReverse,output);
		vbox.getChildren().add(pane);


		Scene scene = new Scene(vbox, 500, 200);
		primaryStage.setTitle("Reverse integer ");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	public int reverseInteger(int integer) {
		if (integer < 10) {
			sb.append(integer);
		} else {
			sb.append(integer % 10);
			reverseInteger(integer / 10);
		}
		return Integer.parseInt(sb.toString());
	}
}


