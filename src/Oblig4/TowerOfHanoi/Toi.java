package Oblig4.TowerOfHanoi;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class Toi extends Application {
	private TextField input = new TextField();
	private Button btnFindMoves = new Button("Kalkuler trekk!");
	private TextArea output = new TextArea();

	private int minMoves;

	public void start(Stage primaryStage) throws Exception {

		FlowPane pane = new FlowPane();
		pane.getChildren().addAll(input,btnFindMoves,output);
		output.setEditable(false);
		output.setMinHeight(500);
		output.setMinWidth(300);
		output.setMaxWidth(300);

		Scene scene = new Scene(pane,300,550);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Tower of Hanoi Calclator");
		primaryStage.show();

		btnFindMoves.setOnAction(event->{
			output.setText("");
			minMoves=0;
			output.appendText("Trekk: \n");
			try {
				if (Integer.parseInt(input.getText())<1) {
					output.appendText("Umulig, du må bruke minst 1 disk");
				} else {
					moveDisks(Integer.parseInt(input.getText()), 'A', 'B', 'C');
					output.appendText("\nMinimum antall trekk er "+ minMoves+"\n\n\n\n\n");
				}
			} catch (NumberFormatException e) {
				output.appendText("INVALID INPUT, skriv in et tall!!");
				output.appendText("\n"+e.getMessage());

			}

		});
	}

	private void moveDisks(int n, char fromTower, char toTower, char auxTower) {
		if (n == 1) {
			output.appendText(String.format("Trekk %4d: flytt disk %4d fra %s til %s\n",
					++minMoves, n, fromTower, toTower));
		} else {
			moveDisks(n - 1, fromTower, auxTower, toTower);
			output.appendText(String.format("Trekk %4d: flytt disk %4d fra %s til %s\n",
					++minMoves, n, fromTower, toTower));
			moveDisks(n - 1, auxTower, toTower, fromTower);
		}
	}
}
