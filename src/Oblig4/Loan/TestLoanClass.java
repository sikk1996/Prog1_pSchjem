package Oblig4.Loan;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class TestLoanClass {
	static ArrayList<Loan> loanList = new ArrayList<>();
	static File savefile = new File("./loanObjects.save");
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Looking for save file...");
		if(loadSave()) {
			System.out.println("Save file found, loading");
		} else {
			System.out.println("Save file not found, first time starting?\n");
		}

		for (Loan loan:loanList) { //print loaded loans
			System.out.println("\n"+loan.toString()+"\n");
		}

		System.out.println("Add a new loan: ");
		System.out.print(
				"Enter yearly interest rate, for example, 8.25: ");
		double annualInterestRate = input.nextDouble();
		System.out.print("Enter number of years as an integer: ");
		int numberOfYears = input.nextInt();
		System.out.print("Enter loan amount, for example, 120000.95: ");
		double loanAmount =  input.nextDouble();
		Loan loan = new Loan(annualInterestRate, numberOfYears, loanAmount);
		System.out.println("\n"+loan.toString()+"\n");

		input.nextLine();
		System.out.println("Save new loan to savefile? [y/n]");
		if (input.nextLine()!="n") {
			loanList.add(loan);
			if(saveObjects()) {
				System.out.println("Loans was saved to file.");
			} else {
				System.out.println("Loans was not saved to file.");
			}

		}
	}

	private static boolean loadSave() {
		boolean status=false;
		try (
				FileInputStream fileInputStream = new FileInputStream(savefile);
				ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
		){
			while (true) {
				loanList.add((Loan)objectInputStream.readObject());
			}
		} catch (EOFException e) {
			status= true; //loaded all objects
		} catch (FileNotFoundException e) {
			status= false;
		}
		catch (Exception e){
			status = false;
			System.out.println("Error loading!"+e.getMessage());
		}
		return status;
	}

	private static boolean saveObjects() {
		try (
				FileOutputStream fileOutputStream = new FileOutputStream(savefile);
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)
		) {
			for (Loan loan : loanList) {
				objectOutputStream.writeObject(loan);
			}
		} catch (Exception e) {
			System.out.println("Error saving: "+ e.getMessage());
			return false;
		}
		return true;

	}
}
