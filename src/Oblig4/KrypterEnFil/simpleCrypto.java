package Oblig4.KrypterEnFil;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.io.*;

public class simpleCrypto extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		TextField inputPath = new TextField();
		inputPath.setMinWidth(150);
		inputPath.setMaxWidth(150);
		inputPath.setText("./plainText.txt");

		TextField outputPath = new TextField();
		outputPath.setMaxWidth(150);
		outputPath.setMinWidth(150);
		outputPath.setText("./output.txt");

		Button btnEncrypt = new Button("Encrypt!");
		btnEncrypt.setMinWidth(300);
		btnEncrypt.setMaxWidth(300);

		TextField message = new TextField("Status: ");
		message.setEditable(false);
		message.setMinWidth(300);
		message.setMinHeight(50);


		btnEncrypt.setOnAction(event->{
			File plainFile = new File(inputPath.getText());
			File cryptedFile = new File(outputPath.getText());
			if(encrypt(plainFile,cryptedFile)) {
				message.setText("Status: Success! "+plainFile.getPath()+" was encrypted to the file "+cryptedFile.getPath());
			} else {
				message.setText("Status: Error!");
			}
		});

		FlowPane pane = new FlowPane();
		pane.getChildren().addAll(inputPath,outputPath,btnEncrypt,message);
		Scene scene = new Scene(pane,300,100);
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
	}



	public static boolean encrypt(File file, File newFile) {
		try (
				BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(file));
				BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(newFile));
		) {
			int buffer = 0;
			while ((buffer = inputStream.read()) != -1) {
				outputStream.write(buffer + 5);
			}
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	public boolean decrypt (File file, File newFile){

		//not part of assignment
		return false;
	}
}
