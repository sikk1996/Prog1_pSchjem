package Oblig4.DecBinHexCONVERTER;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class conv extends Application{

	private static final char HEX_VALUE[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	private static final String HEX_VALUE_STRING = new String(HEX_VALUE);
	private static StringBuilder sb = new StringBuilder();
	private static int sum;

	@Override
	public void start(Stage primaryStage) throws Exception {


		Button btnDecToBin = new Button("Dec to bin");
		Button btnDecToHex = new Button("Dec to hex");
		Button btnBinToDec = new Button("Bin to dec");
		Button btnHexToDec = new Button("Hex to dec");

		TextField input = new TextField();
		TextField output = new TextField();

		output.setEditable(false);

		FlowPane pane = new FlowPane();
		pane.getChildren().addAll(btnDecToBin,btnDecToHex,btnBinToDec,btnHexToDec,input,output);
		pane.setHgap(10);
		pane.setVgap(10);

		Scene scene = new Scene(pane,330,80);

		primaryStage.setScene(scene);
		primaryStage.setTitle("Dec hex bin Converter!");
		primaryStage.show();

		btnDecToBin.setOnAction(event->{sb.setLength(0);
			output.clear();
			output.setText(dec2Bin(Integer.parseInt(input.getText())));
		});
		btnDecToHex.setOnAction(event->{sb.setLength(0);
			output.clear();
			output.setText(dec2Hex(Integer.parseInt(input.getText())));

		});
		btnBinToDec.setOnAction(event->{sb.setLength(0);
			output.clear();
			output.setText(""+bin2Dec(input.getText()));
		});
		btnHexToDec.setOnAction(event->{sb.setLength(0);
			output.clear();
			output.setText(""+hex2Dec(input.getText()));
		});

	}
	private String dec2Bin(int value) {
		if (value > 0) {
			dec2Bin(value / 2);
			sb.append(value % 2);
		}
		return sb.toString();
	}


	private String dec2Hex(int value) {
		if (value > 0) {
			dec2Hex(value / 16);
			sb.append(HEX_VALUE[value % 16]);
		}
		return sb.toString();
	}


	private int bin2Dec(String string) {
		sum = 0;
		return bin2Dec(string, 0, string.length() - 1);
	}
	private int bin2Dec(String string, int low, int high) {
		if (low < high) {
			if (string.charAt(high) - '0' == 1) {
				sum += Math.pow(2, (string.length() - 1) - high);
			}
			bin2Dec(string, low, high - 1);
		} else if (low == high) {
			if (string.charAt(high) - '0' == 1) {
				sum += Math.pow(2, (string.length() - 1) - high);
			}
		}
		return sum;
	}


	private int hex2Dec(String string) {
		sum = 0;
		return hex2Dec(string, 0, string.length() - 1);
	}
	private int hex2Dec(String string, int low, int high) {
		if (low < high) {
			sum += (HEX_VALUE_STRING.indexOf(string.charAt(high))) * Math.pow(16, (string.length() - 1) - high);
			hex2Dec(string, low, high - 1);
		} else if (low == high) {
			sum += (HEX_VALUE_STRING.indexOf(string.charAt(high))) * Math.pow(16, (string.length() - 1) - high);
		}
		return sum;
	}

}
