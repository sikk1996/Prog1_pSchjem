package Oblig3.Sjakkbrett;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Sjakkbrett extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		GridPane gridpane = new GridPane();
		Scene scene = new Scene(gridpane);
		primaryStage.setScene(scene);

		//Rectangle black = new Rectangle(30,30,Color.BLACK);
		//Rectangle white = new Rectangle(30,30,Color.WHITE);
		primaryStage.setTitle("Peters' sjakkbrett");
		primaryStage.setResizable(false);
		for (int i = 0;i<8;i++) {
			for (int e=0; e<8;e++) {
				if ((i+e)%2==0) {
					gridpane.add(new Rectangle(30,30,Color.WHITE),e,i);
				} else {
					gridpane.add(new Rectangle(30,30,Color.BLACK),e,i);
				}
			}
		}
		/*
		gridpane.add(new Rectangle(30,30,Color.WHITE),0,0);
		gridpane.add(new Rectangle(30,30,Color.BLACK),0,1);
		gridpane.add(new Rectangle(30,30,Color.WHITE),0,2);
		gridpane.add(new Rectangle(30,30,Color.BLACK),0,3);
		gridpane.add(new Rectangle(30,30,Color.WHITE),0,4);
		gridpane.add(new Rectangle(30,30,Color.BLACK),0,5);
		gridpane.add(new Rectangle(30,30,Color.WHITE),0,6);
		gridpane.add(new Rectangle(30,30,Color.BLACK),0,7);

		gridpane.add(new Rectangle(30,30,Color.BLACK),1,0);
		gridpane.add(new Rectangle(30,30,Color.WHITE),1,1);
		gridpane.add(new Rectangle(30,30,Color.BLACK),1,2);
		gridpane.add(new Rectangle(30,30,Color.WHITE),1,3);
		gridpane.add(new Rectangle(30,30,Color.BLACK),1,4);
		gridpane.add(new Rectangle(30,30,Color.WHITE),1,5);
		gridpane.add(new Rectangle(30,30,Color.BLACK),1,6);
		gridpane.add(new Rectangle(30,30,Color.WHITE),1,7);

		gridpane.add(new Rectangle(30,30,Color.WHITE),2,0);
		gridpane.add(new Rectangle(30,30,Color.BLACK),2,1);
		gridpane.add(new Rectangle(30,30,Color.WHITE),2,2);
		gridpane.add(new Rectangle(30,30,Color.BLACK),2,3);
		gridpane.add(new Rectangle(30,30,Color.WHITE),2,4);
		gridpane.add(new Rectangle(30,30,Color.BLACK),2,5);
		gridpane.add(new Rectangle(30,30,Color.WHITE),2,6);
		gridpane.add(new Rectangle(30,30,Color.BLACK),2,7);

		gridpane.add(new Rectangle(30,30,Color.BLACK),3,0);
		gridpane.add(new Rectangle(30,30,Color.WHITE),3,1);
		gridpane.add(new Rectangle(30,30,Color.BLACK),3,2);
		gridpane.add(new Rectangle(30,30,Color.WHITE),3,3);
		gridpane.add(new Rectangle(30,30,Color.BLACK),3,4);
		gridpane.add(new Rectangle(30,30,Color.WHITE),3,5);
		gridpane.add(new Rectangle(30,30,Color.BLACK),3,6);
		gridpane.add(new Rectangle(30,30,Color.WHITE),3,7);

		gridpane.add(new Rectangle(30,30,Color.WHITE),4,0);
		gridpane.add(new Rectangle(30,30,Color.BLACK),4,1);
		gridpane.add(new Rectangle(30,30,Color.WHITE),4,2);
		gridpane.add(new Rectangle(30,30,Color.BLACK),4,3);
		gridpane.add(new Rectangle(30,30,Color.WHITE),4,4);
		gridpane.add(new Rectangle(30,30,Color.BLACK),4,5);
		gridpane.add(new Rectangle(30,30,Color.WHITE),4,6);
		gridpane.add(new Rectangle(30,30,Color.BLACK),4,7);

		gridpane.add(new Rectangle(30,30,Color.BLACK),5,0);
		gridpane.add(new Rectangle(30,30,Color.WHITE),5,1);
		gridpane.add(new Rectangle(30,30,Color.BLACK),5,2);
		gridpane.add(new Rectangle(30,30,Color.WHITE),5,3);
		gridpane.add(new Rectangle(30,30,Color.BLACK),5,4);
		gridpane.add(new Rectangle(30,30,Color.WHITE),5,5);
		gridpane.add(new Rectangle(30,30,Color.BLACK),5,6);
		gridpane.add(new Rectangle(30,30,Color.WHITE),5,7);

		gridpane.add(new Rectangle(30,30,Color.WHITE),6,0);
		gridpane.add(new Rectangle(30,30,Color.BLACK),6,1);
		gridpane.add(new Rectangle(30,30,Color.WHITE),6,2);
		gridpane.add(new Rectangle(30,30,Color.BLACK),6,3);
		gridpane.add(new Rectangle(30,30,Color.WHITE),6,4);
		gridpane.add(new Rectangle(30,30,Color.BLACK),6,5);
		gridpane.add(new Rectangle(30,30,Color.WHITE),6,6);
		gridpane.add(new Rectangle(30,30,Color.BLACK),6,7);

		gridpane.add(new Rectangle(30,30,Color.BLACK),7,0);
		gridpane.add(new Rectangle(30,30,Color.WHITE),7,1);
		gridpane.add(new Rectangle(30,30,Color.BLACK),7,2);
		gridpane.add(new Rectangle(30,30,Color.WHITE),7,3);
		gridpane.add(new Rectangle(30,30,Color.BLACK),7,4);
		gridpane.add(new Rectangle(30,30,Color.WHITE),7,5);
		gridpane.add(new Rectangle(30,30,Color.BLACK),7,6);
		gridpane.add(new Rectangle(30,30,Color.WHITE),7,7);
		*/

		primaryStage.show();
	}
}
