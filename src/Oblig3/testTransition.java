package Oblig3;

import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;
import javafx.util.Duration;

public class testTransition extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		// Create a pane
		Pane pane = new Pane();

		// Create a scene and place it in the stage
		Scene scene = new Scene(pane, 250, 200);
		primaryStage.setTitle("PathTransitionDemo"); // Set the stage title
		primaryStage.setScene(scene); // Place the scene in the stage
		primaryStage.show(); // Display the stage

		// Create a rectangle
		Circle rectangle = new Circle (0, 0, 15);
		rectangle.setFill(Color.ORANGE);

		Circle littleCircle = new Circle(5,Color.GREEN);
		littleCircle.centerXProperty().bind(pane.widthProperty().divide(2));
		littleCircle.centerYProperty().bind(pane.heightProperty().divide(9));

		Line line = new Line();
		line.startXProperty().bind(littleCircle.centerXProperty());
		line.startYProperty().bind(littleCircle.centerYProperty());

		line.endXProperty().bind(rectangle.centerXProperty());
		line.endYProperty().bind(rectangle.centerYProperty());

		Arc arc = new Arc(125, 100, 200,200,220,100);
		arc.radiusXProperty().bind(arc.radiusYProperty());
		arc.radiusYProperty().bind(pane.heightProperty().divide(1.5));
		arc.centerXProperty().bind(pane.widthProperty().divide(2));
		arc.centerYProperty().bind(pane.heightProperty().divide(9));
		arc.setFill(Color.TRANSPARENT);
		arc.setType(ArcType.OPEN);
		arc.setStroke(Color.BLACK);

		// Add circle and rectangle to the pane
		//pane.getChildren().add(circle);
		pane.getChildren().add(rectangle);
		pane.getChildren().add(arc);
		pane.getChildren().add(line);
		pane.getChildren().add(littleCircle);

		// Create a path transition
		PathTransition pt = new PathTransition();
		pt.setDuration(Duration.millis(1000));
		pt.setPath(arc);
		pt.setNode(rectangle);
		pt.setOrientation(
				PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
		pt.setCycleCount(Timeline.INDEFINITE);
		pt.setAutoReverse(true);
		pt.play(); // Start animation

		pane.setOnMousePressed(e -> pt.pause());
		pane.setOnMouseReleased(e -> pt.play());

	}
}
