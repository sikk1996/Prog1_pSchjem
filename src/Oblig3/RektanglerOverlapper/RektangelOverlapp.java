package Oblig3.RektanglerOverlapper;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import java.util.Scanner;

public class RektangelOverlapp extends Application{
	public void start(Stage primaryStage) throws Exception {
		Pane pane = new Pane();
		Scene scene = new Scene(pane,500,500);
		primaryStage.setTitle("Overlappende rektangler, or do they...");


		Rectangle rect1 =  createRectangleMenu();
		Rectangle rect2 =  createRectangleMenu();
		Label label = new Label("No rectangles overlap");
		label.translateXProperty().bind(pane.widthProperty().divide(2).subtract(100));
		label.translateYProperty().bind(pane.heightProperty().subtract(20));
		pane.getChildren().addAll(rect1,rect2,label);

		if(contains(rect1,rect2)) {
			label.setText("One rectangle is contained within another");
		} else if (overlaps(rect1,rect2)) {
			label.setText("One rectangle overlaps the other");
		}
		primaryStage.setScene(scene);
		primaryStage.show();

	}
	private static Rectangle createRectangleMenu() {
		Scanner input = new Scanner(System.in);
		System.out.println("Nå skal vi lage et rektangel");
		System.out.println("Skriv inn høyde: ");double height = input.nextDouble();
		System.out.println("Skriv inn bredde ");double width = input.nextDouble();
		System.out.println("Skriv inn senter x-koordinat: ");int x = input.nextInt();
		System.out.println("Skriv inn senter y-koordinat: ");int y = input.nextInt();
		Rectangle rect =  new Rectangle(x,y,width,height);
		rect.setFill(Color.TRANSPARENT);
		rect.setStroke(Color.BLACK);
		rect.setStrokeWidth(4);
		return rect;
	}

	/** Returns true if r1 contains r2 */
	private static boolean contains(Rectangle r1, Rectangle r2) {
		// Four corner points in r2
		double x1 = r2.getX();
		double y1 = r2.getY();
		double x2 = x1 + r2.getWidth();
		double y2 = y1;
		double x3 = x1;
		double y3 = y1 + r2.getHeight();
		double x4 = x1 + r2.getWidth();
		double y4 = y1 + r2.getHeight();

		return r1.contains(x1, y1) && r1.contains(x2, y2) && r1.contains(x3, y3) && r1.contains(x4, y4);
	}

	/** Returns true if r1 overlaps r2 */
	private static boolean overlaps(Rectangle r1, Rectangle r2) {
		// Four corner points in r2
		double r1xCenter = r1.getX() + r1.getWidth() / 2;
		double r2xCenter = r2.getX() + r2.getWidth() / 2;
		double r1yCenter = r1.getY() + r1.getHeight() / 2;
		double r2yCenter = r2.getY() + r2.getHeight() / 2;

		return Math.abs(r1xCenter - r2xCenter) <= (r1.getWidth() + r2.getWidth()) / 2 &&
				Math.abs(r1yCenter - r2yCenter) <= (r1.getHeight() + r2.getHeight()) / 2;
	}
}
