package Oblig3.TegneSirkelOgFirkant;

import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class TegneSirkelOgFirkant extends Application {
	BorderPane borderPane = new BorderPane();
	Scene scene = new Scene(borderPane,500,500);

	VBox leftMenu = new VBox();
	VBox rightMenu = new VBox();
	HBox bottomMenu = new HBox();
	Pane appContent = new Pane();
	VBox topMenu = new VBox();
	Button redBtn = new Button("Rød");
	Button blueBtn = new Button("Blå");
	Button squareBtn = new Button("Firkant");
	Button circleBtn = new Button("Sirkel");
	Button rotBtn = new Button("Roter");
	Text squaretext = new Text("Ingen firkant. ");
	Text circletext = new Text("Ingen sirkel. "); //Create nodes
	public void start(Stage primaryStage) throws Exception {

		leftMenu.setPadding(new Insets(3));
		leftMenu.setSpacing(5);
		rightMenu.setPadding(new Insets(3));
		rightMenu.setSpacing(5);
		topMenu.setPadding(new Insets(3));
		topMenu.setSpacing(5);
		bottomMenu.setPadding(new Insets(3));
		bottomMenu.setSpacing(5);

		leftMenu.setAlignment(Pos.CENTER);
		rightMenu.setAlignment(Pos.CENTER);
		topMenu.setAlignment(Pos.CENTER);
		bottomMenu.setAlignment(Pos.CENTER);
		appContent.setStyle("-fx-border-color: black;");

		borderPane.setLeft(leftMenu);
		borderPane.setRight(rightMenu);
		borderPane.setBottom(bottomMenu);
		borderPane.setCenter(appContent);
		borderPane.setTop(topMenu);

		//ADD TO PANES AND SHOW STAGE
		leftMenu.getChildren().addAll(redBtn,blueBtn);
		rightMenu.getChildren().add(rotBtn);
		bottomMenu.getChildren().addAll(squareBtn,circleBtn);
		topMenu.getChildren().addAll(squaretext,circletext);
		primaryStage.setScene(scene);
		primaryStage.show(); //add nodes to panes,add scene to stage, show stage

		//EVENLISTENERS
		redBtn.setOnAction(e->setColorCurrentShape(Color.RED));
		blueBtn.setOnAction(e->setColorCurrentShape(Color.BLUE));
		squareBtn.setOnAction(e->squareBtnAction());
		circleBtn.setOnAction(e->circleBtnAction());
		rotBtn.setOnAction(e->rotBtnAction());

	}
	void currentShape() {

	}
	void setColorCurrentShape(Paint color) {
		try {
			Shape shape = (Shape)appContent.getChildren().get(appContent.getChildren().size()-1);
			shape.setFill(color);
		} catch (Exception e) {
			System.out.println("You must create the shape first");
		}
	}
	void squareBtnAction(){
		Rectangle rect1 = new Rectangle(40,40);
		rect1.xProperty().bind(appContent.widthProperty().subtract(rect1.widthProperty().multiply(2)).multiply(Math.random()).add(rect1.widthProperty()));
		rect1.yProperty().bind(appContent.heightProperty().subtract(rect1.heightProperty().multiply(2)).multiply(Math.random()).add(rect1.heightProperty()));
		for (int i =0;i<appContent.getChildren().size();i++) {
			if (appContent.getChildren().get(i) instanceof Rectangle) {
				appContent.getChildren().remove(i);
			}
		}
		rect1.setOnMouseClicked(click-> { //Satan så dirty
			appContent.getChildren().remove(rect1);
			appContent.getChildren().add(rect1);
		});
		appContent.getChildren().add(rect1);
		squaretext.setText("Firkant: x-"+rect1.getX()+", y-"+rect1.getY());
	}
	void circleBtnAction(){
		Circle circ1 = new Circle(1,1,20);
		circ1.centerXProperty().bind(appContent.widthProperty().subtract(circ1.radiusProperty().multiply(2)).multiply(Math.random()).add(circ1.radiusProperty()));
		circ1.centerYProperty().bind(appContent.heightProperty().subtract(circ1.radiusProperty().multiply(2)).multiply(Math.random()).add(circ1.radiusProperty()));
		for (int i =0;i<appContent.getChildren().size();i++) {
			if (appContent.getChildren().get(i) instanceof Circle) {
				appContent.getChildren().remove(i);
			}
		}
		circ1.setOnMouseClicked(click-> { //Satan så dirty
			appContent.getChildren().remove(circ1);
			appContent.getChildren().add(circ1);
		});
		appContent.getChildren().add(circ1);
		circletext.setText("Sirkel: x-"+circ1.getCenterX()+", y-"+circ1.getCenterY());
	}
	void rotBtnAction(){
		RotateTransition rt = new RotateTransition();
		rt.setNode(appContent.getChildren().get(appContent.getChildren().size()-1));
		rt.setFromAngle(appContent.getChildren().get(appContent.getChildren().size()-1).getRotate());
		rt.setToAngle(appContent.getChildren().get(appContent.getChildren().size()-1).getRotate()+45);
		rt.setInterpolator(Interpolator.LINEAR);
		rt.setDuration(new Duration(3000));
		rt.play();

		//appContent.getChildren().get(appContent.getChildren().size()-1).setRotate(appContent.getChildren().get(appContent.getChildren().size()-1).getRotate()+45);
	}
}