package Oblig3;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class testArc extends Application{
	@Override
	public void start(Stage primaryStage) throws Exception {

		Arc arc2 = new Arc(150, 100, 80, 80, 30 + 90, 35);
		arc2.setFill(Color.WHITE);
		arc2.setType(ArcType.OPEN);
		arc2.setStroke(Color.BLACK);



		// Create a group and add nodes to the group
		Group group = new Group();
		group.getChildren().addAll(new Text(20, 40, "arc2: open"), arc2);

		// Create a scene and place it in the stage
		Scene scene = new Scene(new BorderPane(group), 300, 200);
		primaryStage.setTitle("ShowArc"); // Set the stage title
		primaryStage.setScene(scene); // Place the scene in the stage
		primaryStage.show(); // Display the stage
	}
}
