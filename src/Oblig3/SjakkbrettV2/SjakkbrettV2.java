package Oblig3.SjakkbrettV2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class SjakkbrettV2 extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		GridPane gridpane = new GridPane();
		Scene scene = new Scene(gridpane,300,300);
		primaryStage.setScene(scene);

		primaryStage.setTitle("Peters' sjakkbrett");
		//primaryStage.setResizable(false);
		Rectangle rect;
		for (int i = 0;i<8;i++) {
			for (int e=0; e<8;e++) {

				if ((i+e)%2==0) {
					rect = new Rectangle(30,30,Color.WHITE);
					rect.widthProperty().bind(scene.widthProperty().divide(7));
					rect.heightProperty().bind(scene.heightProperty().divide(7));
					gridpane.add(rect,e,i);
				} else {
					rect = new Rectangle(30,30,Color.BLACK);
					rect.widthProperty().bind(scene.widthProperty().divide(7));
					rect.heightProperty().bind(scene.heightProperty().divide(7));
					gridpane.add(rect,e,i);
				}
			}
		}


		primaryStage.show();
	}
}
