package Oblig3.Pendel;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

import static javafx.scene.paint.Color.BLACK;

public class Pendel extends Application {
	@Override
	public void start(Stage primaryStage) {
		boolean isPaused;
		PendulumPane pane = new PendulumPane();
		Scene scene = new Scene(pane, 300, 200);
		primaryStage.setTitle("Pendulum"); // Set the stage title
		primaryStage.setScene(scene); // Place the scene in the stage
		primaryStage.show(); // Display the stage

		Timeline animation = new Timeline(new KeyFrame(Duration.millis(100), e -> { pane.next(); }));
		animation.setCycleCount(Timeline.INDEFINITE);
		animation.play(); // Start animation
		isPaused=false;

		pane.setOnMouseClicked(e -> {
			//pause and play anim
			if (animation.getStatus()== Animation.Status.PAUSED) {
				animation.play();
			} else {
				animation.pause();
			}
		});

		pane.requestFocus();
		pane.setOnKeyPressed(e -> {
			switch (e.getCode()) {
				case DOWN:
					animation.setRate(animation.getRate()-2);
					break;
				case UP:
					animation.setRate(animation.getRate()+2);
					break;
				case ESCAPE:
					animation.setRate(1);
					break;
				case SPACE:
					if (animation.getStatus()== Animation.Status.PAUSED) {
						animation.play();
					} else {
						animation.pause();
					}
					break;
			}
		});
	}

	class PendulumPane extends Pane {
		Rotate rot = new Rotate();
		boolean directionRight = true;
		public PendulumPane() {
			this.setWidth(300);this.setHeight(200);
			Circle bigCircle = new Circle(10,BLACK);
			Circle littleCircle = new Circle(5,BLACK);
			littleCircle.setCenterX(this.getWidth()/2);
			littleCircle.setCenterY(30);
			bigCircle.setCenterY(littleCircle.getCenterY());
			bigCircle.setCenterX(this.getWidth()+5);
			Line line = new Line(littleCircle.getCenterX(),littleCircle.getCenterY(),bigCircle.getCenterX(),bigCircle.getCenterY());

			rot.pivotXProperty().bind(littleCircle.centerXProperty());
			rot.pivotYProperty().bind(littleCircle.centerYProperty());
			this.getTransforms().add(rot);
			rot.setAngle(120);
			this.getChildren().addAll(bigCircle,littleCircle,line);

		}

		public void next() {
			double currentRot = rot.getAngle();
			if (directionRight) {
				if(currentRot>=60) {
					rot.setAngle(currentRot-2);
				} else {
					directionRight=false;
				}
			} else {
				if(currentRot<=120) {
					rot.setAngle(currentRot+2);
				} else {
					directionRight=true;
				}
			}




		}
	}
}