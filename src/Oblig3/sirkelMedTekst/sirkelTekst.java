package Oblig3.sirkelMedTekst;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;

public class sirkelTekst extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		String tekst = "Jeg er en grønn sirkel!";
		Pane pane = new Pane();
		Scene scene = new Scene(pane,500,500);
		Circle circle = new Circle();
		primaryStage.setTitle("Peters Sirkel!!!! :D");
		circle.radiusProperty().bind(pane.heightProperty().divide(3));
		circle.centerXProperty().bind(pane.widthProperty().divide(2));
		circle.centerYProperty().bind(pane.heightProperty().divide(2));
		circle.setFill(Color.GREEN);
		circle.setStroke(Color.BLACK);
		circle.setStrokeWidth(2);

		int degrees = 360/tekst.length();
		for (int i = 0;i<tekst.length();i++) {
			Text txt = new Text (tekst.charAt(i)+"");
			txt.xProperty().bind(circle.centerXProperty());
			txt.yProperty().bind(circle.centerYProperty().subtract(circle.radiusProperty()).subtract(9));

			Rotate rot = new Rotate();
			rot.pivotXProperty().bind(circle.centerXProperty());
			rot.pivotYProperty().bind(circle.centerYProperty());
			rot.setAngle(degrees*i);
			txt.getTransforms().add(rot);
			txt.setFont(Font.font("Comic Sans MS", 20));
			pane.getChildren().add(txt);
		}

		pane.getChildren().add(circle);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}