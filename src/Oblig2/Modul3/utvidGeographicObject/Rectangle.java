package Oblig2.Modul3.utvidGeographicObject;

public class Rectangle extends GeometricObject{

    private double width;
    private double height;

    public Rectangle() {
        width=1;
        height=1;
    }
    public Rectangle(double width) {
        this.width = width;
        height=width;
    }
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }
    public Rectangle(double width, double height, String color, boolean filled) {
        super(color, filled);
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }

    public double getArea() {
        return width*height;
    }
    public double getPerimeter() {
        return width*2+height*2;
    }

    public boolean equals(GeometricObject o) {
        return o.getArea()==this.getArea() && o.getPerimeter()==this.getPerimeter() && o.getColor().equals(this.getColor());
    }
    public String toString() {
        return "Rectangle\n"+"Width: "+width+" Height: "+height+super.toString(); //TODO
    }
}
