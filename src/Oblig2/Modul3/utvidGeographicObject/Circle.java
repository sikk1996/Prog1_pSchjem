package Oblig2.Modul3.utvidGeographicObject;

public class Circle extends GeometricObject{
    private double radius;

    public Circle() {
            double radius=1;
    }
    public Circle(double radius) {
            this.radius = radius;
    }
    public Circle(double radius, String color, boolean filled) {
            super(color,filled);
            this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.pow(radius,2)*Math.PI;
    }
    public double getPerimeter() {
        return 2*Math.PI*radius;
    }

    public boolean equals(GeometricObject o) {
        return (o.getArea()==this.getArea() && o.getPerimeter()==this.getPerimeter() && o.getColor() == this.getColor());
    }
    public String toString() {
        return "Circle\n+"+"Radius: "+radius+super.toString(); //TODO
    }
}
