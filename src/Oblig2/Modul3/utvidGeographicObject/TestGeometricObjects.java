package Oblig2.Modul3.utvidGeographicObject;

public class TestGeometricObjects {
    public static void main(String[] args) {

        GeometricObject biggestCircle;
        GeometricObject biggestRectangle;
        GeometricObject biggestTriangle;


        GeometricObject circle1 = new Circle(2);
        GeometricObject circle2 = new Circle(3);

        GeometricObject square1 = new Rectangle(2,3);
        GeometricObject square2 = new Rectangle(10,3);

        GeometricObject triangle1 = new Triangle(1,2,3);
        GeometricObject triangle2 = new Triangle(1);

        biggestCircle = GeometricObject.max(circle1,circle2);
        biggestRectangle = GeometricObject.max(square1,square2);
        biggestTriangle = GeometricObject.max(triangle1,triangle2);

        System.out.println(biggestCircle.toString());
        System.out.println(biggestRectangle.toString());
        System.out.println(biggestTriangle.toString());













    }
}
