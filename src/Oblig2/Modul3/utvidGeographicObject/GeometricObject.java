package Oblig2.Modul3.utvidGeographicObject;

import Oblig2.Modul3.forelesning09022017.Colorable;

import java.time.Year;
import java.util.Calendar;
import java.util.GregorianCalendar;

public abstract class GeometricObject implements Comparable<GeometricObject>{
    private String color = "white";
    private boolean filled;
    private GregorianCalendar dateCreated;

    public GeometricObject() {
        dateCreated = new GregorianCalendar();
    }
    public GeometricObject(String color, boolean filled) {
        dateCreated = new GregorianCalendar();
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public boolean isFilled() {
        return filled;
    }
    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    public GregorianCalendar getDateCreated() {
        return dateCreated;
    }

    public abstract double getArea();
    public abstract double getPerimeter();
    public abstract boolean equals(GeometricObject other);

    public static GeometricObject max(GeometricObject obj1,GeometricObject obj2){
        if (obj1.getArea()<obj2.getArea()){
            return obj2;
        }
        return obj1;
    }
    public String toString() {
        return "\nCreated on " + dateCreated.get(Calendar.YEAR)+"-"+(dateCreated.get(Calendar.MONTH)+1)+"-"+dateCreated.get(Calendar.DAY_OF_MONTH)
                + " "+dateCreated.get(Calendar.HOUR_OF_DAY)+":"+dateCreated.get(Calendar.MINUTE)+":"+dateCreated.get(Calendar.SECOND)
                + "\nColor: " + color
                + "\nIs filled: " + ((filled)?"yes":"no")+"\n";
    }
    public int compareTo(GeometricObject o) {
        if (getArea() > o.getArea())
            return 1;
        if (getArea() == o.getArea())
            return 0;
        return -1;
    }

}
