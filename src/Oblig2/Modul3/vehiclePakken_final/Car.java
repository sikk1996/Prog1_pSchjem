package Oblig2.Modul3.vehiclePakken_final;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * Created by Fredrik Bentzen on feb, 2018
 * Modified by Fredrik Bentzen on feb, 2018
 * Modified by Peter Schjem on feb, 2018
 */

public class Car extends Vehicle {
    private int power;
    private Calendar productionDate;

    public Car() {
        this.productionDate = new GregorianCalendar();
    }

    public Car(String name, String colour, int price, int model, String serialNumber, int direction, int power) {
        super(name, colour, price, model, serialNumber, direction);
        this.power = power;
        this.productionDate = new GregorianCalendar();
    }

    @Override
    public void setAllFields() {
        super.setAllFields();
        System.out.print("Power: ");
        power = input.nextInt();
    }

    public void turnRight(int degrees) {
        while (!(degrees >= 0 && degrees < 360)) {
            if (degrees >= 360) {
                degrees -= 360;
            } else {
                degrees += 360;
            }
        }
        if ((getDirection() + degrees) < 360) {
            setDirection(getDirection() + degrees);
        } else {
            setDirection(getDirection() + degrees - 360);
        }
    }

    public void turnLeft(int degrees) {
        while (!(degrees >= 0 && degrees < 360)) {
            if (degrees >= 360) {
                degrees -= 360;
            } else {
                degrees += 360;
            }
        }
        if ((getDirection() - degrees) >= 0) {
            setDirection(getDirection() - degrees);
        } else {
            setDirection(getDirection() - degrees + 360);
        }
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public Calendar getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(Calendar productionDate) {
        this.productionDate = productionDate;
    }

    @Override
    public void accelerate(int speedFactor) {
        if (getSpeed() == 0) {
            if (0.5 * speedFactor > MAX_SPEED_CAR) {
                setSpeed(MAX_SPEED_CAR);
            } else {
                setSpeed(0.5 * speedFactor);
            }
        } else {
            if (getSpeed() * 0.5 * speedFactor > MAX_SPEED_CAR) {
                setSpeed(MAX_SPEED_CAR);
            } else {
                setSpeed(getSpeed() * 0.5 * speedFactor);
            }
        }
        System.out.printf("Oblig2.Modul3.vehiclePakken_final.Vehicle accelerated to: %.2f\n", getSpeed());
    }

    @Override
    public void breaks(int speedFactor) {
        setSpeed(getSpeed() / speedFactor);
        System.out.printf("Oblig2.Modul3.vehiclePakken_final.Vehicle slowed down to: %.2f\n", getSpeed());
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Car car = (Car) super.clone();
        car.productionDate = (Calendar) car.productionDate.clone();
        return car;
    }

    @Override
    public String toString() {
        return String.format("%s, Power: %d, Production date: %tF\n", super.toString(), power, productionDate);
    }

    @Override
    public void writeData(PrintWriter out) throws IOException {
        super.writeData(out);
        out.write(getPower() + ",");
        out.write(String.format("%tF,\n", getProductionDate()));
    }

    @Override
    public void readData(Scanner in) throws IOException {
        super.readData(in);
        setPower(in.nextInt());
        String tempDate = in.next();
        String[] date = tempDate.split("-",3);
        setProductionDate(new GregorianCalendar(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2])));
        in.nextLine();
    }
}
