package Oblig2.Modul3.vehiclePakken_final;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Class that tests functionality of Oblig2.Modul3.vehiclePakken_final.Vehicle, Oblig2.Modul3.vehiclePakken_final.Bicycle and Car classes.
 * Modified by Fredrik Bentzen on feb, 2018
 * Modified by Peter Schjem on feb, 2018
 */

public class TestVehicles {
    ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
	File file = new File("./saveFiles/vehicles.save");

    public static void main(String[] args) {
        TestVehicles vtest = new TestVehicles();
        try {
            vtest.initializeData();
            vtest.menuLoop();
            vtest.saveData();
        } catch (InputMismatchException e) {
            System.out.println("InputMismatchException!");
            System.out.println(e.getMessage());
            vtest.saveData();
            System.exit(1);
        }
    }

	private void initializeData()  {
        try {
            Scanner in = new Scanner(file).useLocale(Locale.US);
	        in.useDelimiter(",");

	        while (in.hasNext()) {
		        String vehClass = in.next();                    // leser klassenavnet fra filen
		        Class veh1 = Class.forName(vehClass);           // oppretter Class objekt for angitt klassenavn (String)
		        Vehicle veh = (Vehicle)veh1.newInstance();      // oppretter ny instans av Oblig2.Modul3.vehiclePakken_final.Vehicle
		        veh.readData(in);
                System.out.printf("Oblig2.Modul3.vehiclePakken_final.Vehicle read from file: %s", veh);
		        vehicles.add(veh);
            }
	        in.close();
        } catch (FileNotFoundException e) {
	        System.out.println("Savefile not found.");
        } catch (Exception e) {
	        System.out.println("Error: "+e.getMessage());
        }
    }

	private void saveData() {
        Collections.sort(vehicles);
    	try (PrintWriter writer = new PrintWriter(file)){
			for (int i=0;i<vehicles.size();i++) {
				vehicles.get(i).writeData(writer);
                System.out.printf("Oblig2.Modul3.vehiclePakken_final.Vehicle written to file: %s", vehicles.get(i));
			}
		    writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    private void menuLoop() throws InputMismatchException {
        Scanner input = new Scanner(System.in);
        Vehicle vehicle, selectedVehicle;
        String vehicleName;

        while (true) {
	        System.out.println("0..............................Exit program");
            System.out.println("1...................................New car");
            System.out.println("2...............................New bicycle");
            System.out.println("3......................Find vehicle by name");
            System.out.println("4..............Show data about all vehicles");
            System.out.println("5.......Change direction of a given vehicle");
            System.out.println("6.........................Test clone method");
            System.out.println("7..................Test driveable interface");
	        System.out.println("8......BETA.........Clone a vehicle by name");
            System.out.println(".......................................... ");
            System.out.print  ("..............................Your choice? ");

            int choice = input.nextInt();

            switch (choice) {
	            case 0:
		            saveData();
		            input.close();
		            System.exit(0);
            	case 1:
                    System.out.println("Input car data:");
                    vehicle = new Car();
                    vehicle.setAllFields(); // Override method (done)
                    vehicles.add(vehicle);
                    break;
                case 2:
                    System.out.println("Input bicycle data:");
                    vehicle = new Bicycle();
                    vehicle.setAllFields(); // Override method (done)
                    vehicles.add(vehicle);
                    break;
                case 3:
                    // ask for vehicle name and print out vehicle with that name (done)
                    input.nextLine();
                    System.out.print("Name of vehicle: ");
                    vehicleName = input.nextLine();
                    selectedVehicle = getVehicleByVehicleName(vehicleName);
                    if (selectedVehicle != null) {
                        System.out.print(selectedVehicle.toString());
                    } else {
                        System.out.println("No vehicle with the specified name");
                    }
                    break;
                case 4:
                    // list all vehicles (done)
                    for (Vehicle aVehicle : vehicles) {
                        System.out.print(aVehicle.toString());
                    }
                    break;
                case 5:
                    // ask for vehicle name and change its direction (done)
                    input.nextLine();
                    System.out.print("Name of vehicle: ");
                    vehicleName = input.nextLine();
                    selectedVehicle = getVehicleByVehicleName(vehicleName);
                    if (selectedVehicle != null) {
                        System.out.print("Direction [R/L]: ");
                        char direction = input.next().charAt(0);
                        switch (direction) {
                            case 'l':
                            case 'L':
                                System.out.print("Degrees [0-360]: ");
                                selectedVehicle.turnLeft(input.nextInt());
                                input.nextLine();
                                break;
                            case 'r':
                            case 'R':
                                System.out.print("Degrees [0-360]: ");
                                selectedVehicle.turnRight(input.nextInt());
                                input.nextLine();
                                break;
                            default:
                                System.out.println("Invalid option!");
                                break;
                        }
                    } else {
                        System.out.println("No vehicle with the specified name");
                    }
                    break;
                case 6:
                    vehicle = new Car();
                    try {
                        Vehicle vehicle2 = (Vehicle) vehicle.clone();
                        System.out.println("Date objects are separate, deep copy.");
                        vehicle2.setBuyingDate(new GregorianCalendar(2000, 1,1));
                        System.out.printf("%tF\n", vehicle.getBuyingDate());
                        System.out.printf("%tF\n", vehicle2.getBuyingDate());
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 7:
                    Vehicle car = new Car();
                    Vehicle bicycle = new Bicycle();
                    System.out.println("Oblig2.Modul3.vehiclePakken_final.Car:");
                    car.accelerate(10);
                    car.accelerate(100);
                    car.breaks(1000);
                    car.stop();
                    System.out.println("Oblig2.Modul3.vehiclePakken_final.Bicycle:");
                    bicycle.accelerate(10);
                    bicycle.accelerate(67);
                    bicycle.breaks(1000);
                    bicycle.stop();
                    break;
	            case 8:
		            input.nextLine();
		            System.out.print("Name of vehicle: ");
		            vehicleName = input.nextLine();
		            selectedVehicle = getVehicleByVehicleName(vehicleName);
		            if (selectedVehicle != null) {
		            	try {
		            		Vehicle tempVehicle = (Vehicle)selectedVehicle.clone();
				            System.out.println("What woud you like to name the clone: ");
				            tempVehicle.setName(input.nextLine());
				            System.out.println("The vehicle was cloned succesfully, and was named '"+tempVehicle.getName()+"'");
				            vehicles.add(tempVehicle);
			            } catch(CloneNotSupportedException e) {
				            System.out.println("Error cloning, object is not cloneable");
			            }
		            } else {
			            System.out.println("The vehicle was not found.");
		            }
		            break;
                default:
                    System.out.println("Invalid option!");
            }
        }
    }

    private Vehicle getVehicleByVehicleName(String vehicleName) {
        for (Vehicle vehicle : vehicles) {
            if (vehicle.getName().equals(vehicleName))
                return vehicle;
        }
        return null;
    }
}