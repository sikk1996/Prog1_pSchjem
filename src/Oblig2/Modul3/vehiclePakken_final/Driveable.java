package Oblig2.Modul3.vehiclePakken_final;

/**
 * Created  by Fredrik Bentzen on feb, 2018
 */

public interface Driveable {
    double MAX_SPEED_CAR = 250.00;
    double MAX_SPEED_BIKE = 100.00;

    void accelerate(int speedFactor);

    void breaks(int speedFactor);

    void stop();
}
