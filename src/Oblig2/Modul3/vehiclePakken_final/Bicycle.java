package Oblig2.Modul3.vehiclePakken_final;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * Created  by Fredrik Bentzen on feb, 2018
 * Modified by Fredrik Bentzen on feb, 2018
 * Modified by Peter Schjem on feb, 2018
 */

public class Bicycle extends Vehicle {
    private int gears;
    private Calendar productionDate;

    public Bicycle() {
        this.productionDate = new GregorianCalendar();
    }

    public Bicycle(String name, String colour, int price, int model, String serialNumber, int direction, int gears) {
        super(name, colour, price, model, serialNumber, direction);
        this.gears = gears;
        this.productionDate = new GregorianCalendar();
    }

    @Override
    public void setAllFields() {
        super.setAllFields();
        System.out.print("Gears: ");
        gears = input.nextInt();
    }

    public void turnRight(int degrees) {
        System.out.print("Turning right (" + degrees + " degrees)\n");
    }

    public void turnLeft(int degrees) {
        System.out.print("Turning left (" + degrees + " degrees)\n");
    }

    public int getGears() {
        return gears;
    }

    public void setGears(int gears) {
        this.gears = gears;
    }

    public Calendar getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(Calendar productionDate) {
        this.productionDate = productionDate;
    }

    @Override
    public void accelerate(int speedFactor) {
        if (getSpeed() == 0) {
            if (0.3 * speedFactor > MAX_SPEED_BIKE) {
                setSpeed(MAX_SPEED_BIKE);
            } else {
                setSpeed(0.3 * speedFactor);
            }
        } else {
            if (getSpeed() * 0.5 * speedFactor > MAX_SPEED_BIKE) {
                setSpeed(MAX_SPEED_BIKE);
            } else {
                setSpeed(getSpeed() * 0.5 * speedFactor);
            }
        }
        System.out.printf("Oblig2.Modul3.vehiclePakken_final.Vehicle accelerated to: %.2f\n", getSpeed());
    }

    @Override
    public void breaks(int speedFactor) {
        setSpeed(getSpeed() / (0.5 * speedFactor));
        System.out.printf("Vehicle slowed down to: %.2f\n", getSpeed());
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Bicycle bicycle = (Bicycle) super.clone();
        bicycle.productionDate = (Calendar) bicycle.productionDate.clone();
        return bicycle;
    }

    @Override
    public String toString() {
        return String.format("%s, Gears: %d, Production date: %tF\n", super.toString(), gears, productionDate);
    }

    @Override
    public void writeData(PrintWriter out) throws IOException {
        super.writeData(out);
        out.write(getGears() + ",");
        out.write(String.format("%tF,\n", getProductionDate()));

    }

    @Override
    public void readData(Scanner in) throws IOException {
        super.readData(in);
        setGears(in.nextInt());
        String tempDate = in.next();
        String[] date = tempDate.split("-",3);
        setProductionDate(new GregorianCalendar(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2])));
        in.nextLine();
    }
}
