package Oblig2.Modul3.vehiclePakken_final;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * Created  by Fredrik Bentzen on feb, 2018
 * Modified by Fredrik Bentzen on feb, 2018
 * Modified by Peter Schjem on feb, 2018
 */

public abstract class Vehicle implements Comparable<Vehicle>, Cloneable, Driveable,Fileable {
    private String colour, name, serialNumber;
    private int model, price, direction; //model is year
    private double speed;
    private Calendar buyingDate;
    protected java.util.Scanner input = new java.util.Scanner(System.in);

    public Vehicle() {
        speed = 0;
        buyingDate = new GregorianCalendar();
    }

    public Vehicle(String name, String colour, int price, int model, String serialNumber, int direction) {
        this();
        this.colour = colour;
        this.name = name;
        this.model = model;
        this.price = price;
        this.direction = direction;
    }

    public void setAllFields() {
        System.out.print("Name: ");
        name = input.nextLine();
        System.out.print("Model: ");
        model = input.nextInt();
        input.nextLine();
        System.out.print("Colour: ");
        colour = input.nextLine();
        System.out.print("Serial number: ");
        serialNumber = input.nextLine();
        System.out.print("Price: ");
        price = input.nextInt();
        input.nextLine();
    }

    public abstract void turnLeft(int degrees);

    public abstract void turnRight(int degrees);

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public Calendar getBuyingDate() {
        return buyingDate;
    }

    public void setBuyingDate(Calendar buyingDate) {
        this.buyingDate = buyingDate;
    }

    @Override
    public void stop() {
        speed = 0;
        System.out.println("Oblig2.Modul3.vehiclePakken_final.Vehicle stops");
    }

    @Override
    public int compareTo(Vehicle o) {
        return Integer.compare(price, o.price);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Vehicle vehicle = (Vehicle) super.clone();
        vehicle.buyingDate = (Calendar) vehicle.buyingDate.clone();
        return vehicle;
    }

    @Override
    public String toString() {
        return String.format("Name: %s, Model: %d, Colour: %s, Serial number: %s, Price: %d, Speed: %.2f, Direction: %d, Buying date: %tF",
                            name, model, colour, serialNumber, price, speed, direction, buyingDate);
    }

    @Override
    public void writeData(PrintWriter out) throws IOException {
        out.write(getClass().getName() + ",");
        out.write(getName() + ",");
        out.write(getColour() + ",");
        out.write(getPrice() + ",");
        out.write(getModel() + ",");
        out.write(getSerialNumber() + ",");
        out.write(getDirection() + ",");
        out.write(getSpeed() + ",");
        out.write(String.format("%tF,", getBuyingDate()));
    }

    @Override
    public void readData(Scanner in) throws IOException {
        setName(in.next());
        setColour(in.next());
        setPrice(in.nextInt());
        setModel(in.nextInt());
        setSerialNumber(in.next());
        setDirection(in.nextInt());
        setSpeed(in.nextDouble());
        String tempDate = in.next();
        String[] date = tempDate.split("-",3);
        setBuyingDate(new GregorianCalendar(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2])));
    }

}
