package Oblig2.Modul3.vehiclePakken;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Car extends Vehicle {
	private int power;
	private Calendar productionDate;

	public Car() {
		super();
		power =0;
		productionDate=new GregorianCalendar();
	}

	public Car(String name,String colour,  int price, int model, String serialNumber, int direction, int power) {
		super(name,colour, price, model, serialNumber,direction);
		this.power = power;
		this.productionDate = new GregorianCalendar();
	}

	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public Calendar getProductionDate() {
		return productionDate;
	}
	public void setProductionDate(Calendar productionDate) {
		this.productionDate = productionDate;
	}

	@Override
	public void setAllFields() {
		super.setAllFields();
		System.out.print("Power: ");power=input.nextInt();
	}

	@Override
	public void turnLeft(int degrees) {
			while(degrees>=360) {
				degrees -=360;
			}
			setDirection(degrees);
	}
	@Override
	public void turnRight(int degrees) {
		while(degrees<0) {
			degrees +=360;
		}
		setDirection(degrees);
	}
	@Override
	public String toString() {
		return super.toString()+
				", Power: " + power +
				", Produksjonsdato: " + format(productionDate);
	}

	@Override
	protected Car clone() throws CloneNotSupportedException {
		Car tempCar = (Car)super.clone();
		tempCar.productionDate = (Calendar) tempCar.productionDate.clone();
		return tempCar;
	}

	public void accelerate(int speedFactor) {
		double newSpeed;
		if(getSpeed()==0) {
			newSpeed= 0.5 * speedFactor;
		} else {
			newSpeed=getSpeed()*speedFactor;
		} if (newSpeed>MAX_SPEED_CAR) {
			newSpeed=MAX_SPEED_CAR;
		}

		setSpeed(newSpeed);
		System.out.println("Vehicle accelerated to: "+getSpeed()+" km/h");
	}

	public void breaks(int speedFactor) {
		setSpeed(getSpeed()/speedFactor);
		System.out.println("Vehicle slowed down to to: "+getSpeed()+" km/h");
	}
}
