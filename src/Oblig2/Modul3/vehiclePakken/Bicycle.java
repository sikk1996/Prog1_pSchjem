package Oblig2.Modul3.vehiclePakken;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Bicycle extends Vehicle {
	private int gears;
	private Calendar productionDate;

	public Bicycle() {
		super();
		gears =0;
	}
	public Bicycle(String name,String colour,  int price, int model, String serialNumber, int direction,int gears) {
		super(name,colour,  price, model, serialNumber,direction);
		this.gears = gears;
		this.productionDate = new GregorianCalendar();
	}

	public int getGears() {
		return gears;
	}

	public void setGears(int gears) {
		this.gears = gears;
	}

	public Calendar getProductionDate() {
		return productionDate;
	}

	public void setProductionDate(Calendar productionDate) {
		this.productionDate = productionDate;
	}

	@Override
	public void turnLeft(int degrees) {
		System.out.println("The bicycle turned "+degrees+" to the left");
	}

	@Override
	public void turnRight(int degrees) {
		System.out.println("The bicycle turned "+degrees+" to the right");

	}
	@Override
	public void setAllFields() {
		super.setAllFields();
		System.out.print("Gears: ");gears=input.nextInt();
	}
	@Override
	public String toString() {
		return super.toString()+
				", Gears: " + gears +
				", Produksjonsdato: " + format(productionDate);
	}
	@Override
	protected Bicycle clone() throws CloneNotSupportedException {
		Bicycle tempBicycle = (Bicycle) super.clone();
		tempBicycle.productionDate = (Calendar) tempBicycle.productionDate.clone();
		return tempBicycle;
	}

	@Override
	public void accelerate(int speedFactor) {
		double newSpeed;
		if(getSpeed()==0) {
			newSpeed= 0.3 * speedFactor;
		} else {
			newSpeed=getSpeed()*0.5*speedFactor;
		}

		if (newSpeed>MAX_SPEED_BIKE) {
			newSpeed=MAX_SPEED_BIKE;
		}
		setSpeed(newSpeed);
		System.out.println("Vehicle accelerated to: "+getSpeed()+" km/h");
	}

	@Override
	public void breaks(int speedFactor) {
		setSpeed(getSpeed() / (speedFactor * 0.5));
		System.out.println("Vehicle slower down to: "+getSpeed()+" km/h");
	}
}

