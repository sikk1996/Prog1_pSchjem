package Oblig2.Modul3.vehiclePakken;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public abstract class Vehicle implements Cloneable,Comparable<Vehicle>,Driveable{
	private String colour,name,serialNumber;
	private int model, price, direction;
	private double speed;
	protected Scanner input = new Scanner(System.in);
	private Calendar buyingDate;

	public Vehicle() {
		name="Ukjent navn";
		colour="Ukjent farge";
		serialNumber = "Ukjent serial #";
		model = Calendar.YEAR;
		price = 1;
		buyingDate = new GregorianCalendar();
	}
	public Vehicle(String name,String colour,  int price, int model, String serialNumber, int direction) {
		this.colour = colour;
		this.name = name;
		this.serialNumber = serialNumber;
		this.model = model;
		this.price = price;
		this.direction = direction;
		this.speed = 0;
		buyingDate = new GregorianCalendar();
	}

	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public int getModel() {
		return model;
	}
	public void setModel(int model) {
		this.model = model;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int direction) {
		this.direction = direction;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public Calendar getBuyingDate() {
		return buyingDate;
	}
	public void setBuyingDate(Calendar buyingDate) {
		this.buyingDate = buyingDate;
	}

	public static String format(Calendar calendar){ //Kunne brukt string format %tF men eh
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
		fmt.setCalendar(calendar);
		String dateFormatted = fmt.format(calendar.getTime());
		return dateFormatted;
	}
	public void setAllFields() {
		System.out.print("Name: ");name=input.nextLine();
		System.out.print("Colour: ");colour=input.nextLine();
		System.out.print("Price: ");price=input.nextInt();input.nextLine();
		System.out.print("Model: ");model=input.nextInt();input.nextLine();
		System.out.print("Serial #: ");serialNumber=input.nextLine();
	}
	public abstract void turnLeft(int degrees);
	public abstract void turnRight(int degrees);
	public String toString() {
		return "Name: "+name+", Colour: "+colour+", Price: "+price+", Serial #:"+serialNumber+
				", Direction: "+direction+", Speed: "+speed;
	}

	public int compareTo(Vehicle o) {
		if (this.price > o.getPrice()) {
			return 1;
		} else if (this.price == o.getPrice()) {
			return 0;
		} return -1;
	}

	@Override
	protected Vehicle clone() throws CloneNotSupportedException {
		Vehicle tempVehicle = (Vehicle)super.clone();
		tempVehicle.buyingDate = (Calendar) tempVehicle.buyingDate.clone();
		return tempVehicle;
	}

	@Override
	public void stop() {
		speed=0;
		System.out.println("Vehicle stopped.");
	}
}
