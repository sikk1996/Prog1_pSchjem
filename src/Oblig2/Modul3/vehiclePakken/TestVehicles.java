package Oblig2.Modul3.vehiclePakken;
import java.util.*;

/**
 * Class that tests functionality of Vehicle, Bicycle and Car classes.
 */

public class TestVehicles {
	ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();

	public static void main(String[] args) {
		TestVehicles vtest = new TestVehicles();
		try {
			vtest.menuLoop();
		} catch (InputMismatchException e) {
			System.out.println("InputMismatchException!");
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}

	private void menuLoop() throws InputMismatchException {
		Scanner input = new Scanner(System.in);
		Vehicle vehicle;
		int vehicleSpot;
		vehicles.add(new Car("Volvo 740", "blue", 85000, 1985, "1010-11", 0, 120));
		vehicles.add(new Car("Ferrari Testarossa", "red", 1200000, 1996, "A112", 0, 350));
		vehicles.add(new Bicycle("Monark 1", "yellow", 4000, 1993, "BC100", 0, 10));
		vehicles.add(new Bicycle("DBS 2", "pink", 5000, 1994, "42", 0, 10));

		while (true) {
			System.out.println("1...................................New car");
			System.out.println("2...............................New bicycle");
			System.out.println("3......................Find vehicle by name");
			System.out.println("4..............Show data about all vehicles");
			System.out.println("5.......Change direction of a given vehicle");
			System.out.println("6.........................Test clone method");
			System.out.println("7..................Test driveable interface");
			System.out.println("8..............................Exit program");
			System.out.print(".............................Your choice? ");

			int choice = input.nextInt();input.nextLine();

			switch (choice) {

				case 1:
					System.out.println("Input car data:");
					vehicle = new Car();
					vehicle.setAllFields();
					vehicles.add(vehicle);
					break;
				case 2:
					System.out.println("Input bicycle data:");
					vehicle = new Bicycle();
					vehicle.setAllFields();
					vehicles.add(vehicle);
					break;
				case 3:
					System.out.println("Input the vehicle name: ");
					//System.out.println(vehicles.get(getVehicleName(input.nextLine())).toString());
					vehicleSpot = getVehicleName(input.nextLine());
					if (vehicleSpot<0) {
						System.out.println("Vehicle not found");
						System.exit(2);
					}
					System.out.println(vehicles.get(vehicleSpot).toString());
					break;
				case 4:
					for (int i=0;i<vehicles.size();i++) {
						System.out.println(vehicles.get(i).toString());
					}
					break;
				case 5:
					System.out.println("Vehicle name:");
					vehicleSpot = getVehicleName(input.nextLine());
					if (vehicleSpot<0) {
						System.out.println("Vehicle not found");
						System.exit(2);
					}
					System.out.println("Left or right?");
					switch(input.nextLine()) {
						case "right":
						case "Right":
						case "r":
						case "R":
							System.out.println("How many degrees?");
							vehicles.get(vehicleSpot).turnRight(input.nextInt());input.nextLine();
							break;
						case "left":
						case "Left":
						case "l":
						case "L":
							System.out.println("How many degrees?");
							vehicles.get(vehicleSpot).turnLeft(input.nextInt());input.nextLine();
							break;
					}

					break;
				case 6:
					try {
						Car bil_1 = new Car("PETERS BIL", "blue", 85000, 1985, "1010-11", 0, 120);
						Car bil_2 = bil_1.clone();
						bil_2.setProductionDate(new GregorianCalendar(1996,10,29));
						System.out.println(bil_1.toString());
						System.out.println(bil_2.toString());
						System.out.println();
					} catch (CloneNotSupportedException e) {
						System.out.println("Error i kloning");
						System.out.println(e.getLocalizedMessage());
					}

					break;
				case 7:
					Car car1 = new Car("PETERS BIL", "blue", 85000, 1985, "1010-11", 0, 120);
					Bicycle bike1 = new Bicycle("DBS 2", "pink", 5000, 1994, "42", 0, 10);

					System.out.println("Car:");
					car1.accelerate(21);
					car1.accelerate(7);
					car1.breaks(24);
					bike1.stop();

					System.out.println("Bike: ");
					bike1.accelerate(21);
					bike1.accelerate(3);
					bike1.breaks(14);
					bike1.stop();

					break;
				case 8:
					input.close();
					System.exit(0);
				default:
					System.out.println("Invalid option!");
			}
		}
	}
	public int getVehicleName(String vehicleName) {
		Scanner input = new Scanner(System.in);
		for (int i = 0;i<vehicles.size();i++) {
			//System.out.println(vehicles.get(i).getName());
			if (vehicleName.equals(vehicles.get(i).getName())) {
				return i;
			}
		}
		return -1;
	}
}