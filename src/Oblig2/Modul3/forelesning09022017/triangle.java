package Oblig2.Modul3.forelesning09022017;

public class triangle extends GeometricObject {
    int side1;
    int side2;
    int side3;

    public triangle(int side1, int side2, int side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public int getSide1() {
        return side1;
    }

    public void setSide1(int side1) {
        this.side1 = side1;
    }

    public int getSide2() {
        return side2;
    }

    public void setSide2(int side2) {
        this.side2 = side2;
    }

    public int getSide3() {
        return side3;
    }

    public void setSide3(int side3) {
        this.side3 = side3;
    }
    public double getArea() {
        return 69;
    }
    public void howToColor() {
        System.out.println( "just fil her up big boy");
    }
}
