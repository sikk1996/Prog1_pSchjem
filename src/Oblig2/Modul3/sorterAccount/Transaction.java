package Oblig2.Modul3.sorterAccount;

import java.util.GregorianCalendar;

public class Transaction {

	private double amount;
	private double balance;
	private String description;
	private GregorianCalendar date;
	private char type;

	public Transaction(char type, double amount, double balance, String desc) {
		this.amount=amount;
		this.balance=balance;
		this.description=desc;
		this.type = type;
		this.date = new GregorianCalendar();

	}

	public double getAmount() {
		return amount;
	}
	public double getBalance() {
		return balance;
	}
	public String getDesciption() {
		return description;
	}
	public GregorianCalendar getDate() {
		return date;
	}
	public char getType() {
		return type;
	}

	@Override
	public String toString() {
		return date+"   "+type+"      "+amount+"     "+balance+"    "+description;
		// return String.format("%30",date,type,amount,balance,description)
	}
}
