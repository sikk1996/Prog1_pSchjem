package Oblig2.Modul3.sorterAccount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;

public class TestAccount {
	public static void main(String[] args) {
		/*Account peter = new Account("Peter",1,2000);
		peter.withdraw(200,"Bolle i ovnen");
		peter.deposit(400,"Lønn");
		peter.withdraw(20,"parkering");
		peter.withdraw(1000,"Joikeboller på statoil");
		peter.deposit(40,"penger fra farfar");
		peter.deposit(1000,"skattepenger");

		printTransactions(peter);*/
		ArrayList<Account> accountList = new ArrayList<>();

		accountList.add(new Account("Peter",1,2000,new GregorianCalendar(2013,10,30)));
		accountList.add(new Account("Jokke",2,1500,new GregorianCalendar(2013,11,4)));
		accountList.add(new Account("Kim",3,2000,new GregorianCalendar(2016,2,3)));
		accountList.add(new Account("Geirarne",4,540,new GregorianCalendar(2015,4,5)));


		System.out.println("Unsoorted:");
		for (Account account:accountList) {
			System.out.println(account.toString());
		}
		System.out.println();

		Collections.sort(accountList);

		System.out.println("Sorted:");
		for (Account account:accountList) {
			System.out.println(account.toString());
		}

	}
	public static void printTransactions(Account account) {
		ArrayList<Transaction> transactions= account.getTransaction();

		System.out.println("Name: "+account.getName());
		System.out.println("Annual interest rate: "+account.getAnnualInterestRate());
		System.out.println("Balance: "+account.getBalance());
		System.out.println("Date                           Type   Amount    Balance   Description");
		for (int i=0;i<transactions.size();i++) {
			System.out.println(transactions.get(i).toString());
		}
	}
}
