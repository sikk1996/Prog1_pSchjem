package Oblig2.Modul3.sorterAccount;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Account implements Comparable<Account>{

	public static final char withdrawal = 'w';
	public static final char deposit = 'd';

	private String name;
	private double balance;
	private int id;
	private static int newID;
	private static double  annualInterestRate;
	private ArrayList<Transaction> transactions = new ArrayList<>(0);
	private GregorianCalendar dateCreated;

	public Account() {
		this("Intet navn",newID,2000,new GregorianCalendar());
	}
	public Account(String name, int id, double balance) {
		this(name,id,balance,new GregorianCalendar());
	}
	public Account(String name, int id, double balance,GregorianCalendar dateCreated) {
		newID++;
		this.name=name;
		this.id=id;
		this.balance=balance;
		this.dateCreated = dateCreated;
	}

	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public GregorianCalendar getDateCreated() {
		return dateCreated;
	}
	public double getAnnualInterestRate() {
		return annualInterestRate;
	}
	public void setAnnualInterestRate(double annualInterestRate) {
		Account.annualInterestRate = annualInterestRate;
	}
	public double geMonthlyInterestRate() {
		return annualInterestRate/12;
	}
	public ArrayList<Transaction> getTransaction() {
		return transactions;
	}

	private String date2String(GregorianCalendar date) { //fuck simpledateformat
		return date.get(Calendar.YEAR)+"-"+(date.get(Calendar.MONTH)+1)+"-"+date.get(Calendar.DAY_OF_MONTH)
				+ " "+date.get(Calendar.HOUR_OF_DAY)+":"+date.get(Calendar.MINUTE)+":"+date.get(Calendar.SECOND);
	}

	public void withdraw(double amount, String name) {

		if (balance>amount) {
			balance -= amount;
		}

		transactions.add(new Transaction(withdrawal,amount,balance,name));
	}
	public void deposit(double amount, String name) {

		if(amount>0) {
			balance += amount;
		}

		transactions.add(new Transaction(deposit,amount,balance,name));
	}

	@Override
	public int compareTo(Account o) {
		if (this.getBalance() > o.getBalance()) {
			return 1;
		} if (this.getBalance() == o.getBalance()) {
			return this.getDateCreated().compareTo(o.getDateCreated());
			//return 0;
		}
		return 0;
	}
	@Override
	public String toString() {
		return "Name: "+getName()+", ID: "+getId()+", Balance: "+getBalance()+", Date Created: "+date2String(dateCreated);
	}
}
